/*
 * Navigation screen control system
 * (C) Cedric PAILLE 2018
 */

// Tested on boad -> OK PB1 -> GO IN
#define MOTOR_FWD_PIN       PB0
#define MOTOR_BWD_PIN       PB1
//
#define MOTOR_FEEDBACK_PIN  PD3
#define STOP_SWITCH_PIN     PB3
#define APC_POWER_PIN       PD0
#define EMERGENCY_LED_PIN   PB4
#define LCD_BACKLIGHT_PIN   PD1

#define APC_TIMEOUT 5000
#define FEEDBACK_TIME_TOO_LONG() ((millis() - LAST_FEEDBACK_TIME) > 100)
#define TOTAL_TIME_TOO_LONG() ((millis() - LAST_ACTION_TIME) > 10000)

enum MOTOR_ACTION {
  STOPPED_IN = 0,
  STOPPED_OUT,
  STOPPED_UNDEF,
  GO_OUT,
  GO_IN,
};

static volatile unsigned int  FEEDBACK_PULSE_COUNT = 0;
static volatile unsigned long LAST_FEEDBACK_TIME = 0;
static unsigned long APC_TIMER = 0;
static unsigned long LAST_ACTION_TIME=0;
static unsigned long MOTOR_UNDEFINED_POS_TIME = 0;
static unsigned int FEEDBACK_PULSE_COUNT_MEM = 0;
static unsigned int FEEDBACK_PULSE_MAX_MEM = 0;
static int UNDEF_RETRIES = 0;
static int CURRENT_MOTOR_ACTION;
static int IS_INITIALIZING;
static int APC_STATUS_MEM = 0;
static int MOTOR_STATUS_MEM = 0;

inline void do_motor_fwd()
{
  digitalWrite(MOTOR_BWD_PIN, LOW);
  digitalWrite(MOTOR_FWD_PIN, HIGH);
}

inline void do_motor_bwd()
{
  digitalWrite(MOTOR_FWD_PIN, LOW);
  digitalWrite(MOTOR_BWD_PIN, HIGH);
}

inline void do_motor_stop()
{
  digitalWrite(MOTOR_FWD_PIN, LOW);
  digitalWrite(MOTOR_BWD_PIN, LOW);
}

/*
 * We're using an ISR to count feedback pulses
 * This allows us to be sure to not miss a state change
 */
void feedback_pulse_isr()
{
  FEEDBACK_PULSE_COUNT++;
  LAST_FEEDBACK_TIME = millis();
}

inline int read_apc()
{
  return digitalRead(APC_POWER_PIN);
}

inline int read_stop_switch()
{
  return digitalRead(STOP_SWITCH_PIN);
}

inline void emergency(int mode){
  if (mode)
    digitalWrite(EMERGENCY_LED_PIN, HIGH);
  else
    digitalWrite(EMERGENCY_LED_PIN, LOW);
}


inline void backlight_lcd(int mode){
  if (mode)
    digitalWrite(LCD_BACKLIGHT_PIN, HIGH);
  else
    digitalWrite(LCD_BACKLIGHT_PIN, LOW);
}

void setup() {
  /*
   * Setup motor to stop mode first to avoid undefined behavior
   */
  pinMode(MOTOR_FWD_PIN, OUTPUT);
  pinMode(MOTOR_BWD_PIN, OUTPUT);
  do_motor_stop();
  
  pinMode(MOTOR_FEEDBACK_PIN, INPUT);
  pinMode(STOP_SWITCH_PIN, INPUT_PULLUP);
  pinMode(APC_POWER_PIN, INPUT);
  pinMode(EMERGENCY_LED_PIN, OUTPUT);
  pinMode(LCD_BACKLIGHT_PIN, OUTPUT);
  emergency(0);
  attachInterrupt(digitalPinToInterrupt(MOTOR_FEEDBACK_PIN), feedback_pulse_isr, CHANGE);
  /*
   * We want the screen to go inside at startup to detect end switch
   */
  CURRENT_MOTOR_ACTION = GO_IN;
  IS_INITIALIZING = 1;
  APC_TIMER = 0;
  MOTOR_UNDEFINED_POS_TIME = 0;
  UNDEF_RETRIES = 0;
  LAST_ACTION_TIME=0;
}

void loop() {
  /*
   * Do not start APC check input until screen is completely inside
   */
  if (!IS_INITIALIZING){
    int apc_status = read_apc();
    if (apc_status != APC_STATUS_MEM){
      APC_TIMER = millis();
      if (APC_TIMER == 0)
        APC_TIMER = 1;
    }
  
    APC_STATUS_MEM = apc_status;
  }
  
  /*
   * Wait 5 seconds before doing any motor action
   * This avoid any unnecessary in/out movement
   */
  if (APC_TIMER != 0 && UNDEF_RETRIES==0){
    if ((millis() - APC_TIMER) > APC_TIMEOUT){
      if (APC_STATUS_MEM == 1){
        if (CURRENT_MOTOR_ACTION != STOPPED_OUT){
          CURRENT_MOTOR_ACTION = GO_OUT;
          FEEDBACK_PULSE_COUNT = 0;
          LAST_FEEDBACK_TIME = millis();
          LAST_ACTION_TIME = millis();
        }
      } else {
        if (CURRENT_MOTOR_ACTION != STOPPED_IN){
          CURRENT_MOTOR_ACTION = GO_IN;
          FEEDBACK_PULSE_COUNT = 0;
          LAST_FEEDBACK_TIME = millis();
          LAST_ACTION_TIME = millis();
        }
      }
      APC_TIMER = 0;
    }
  }

  if (CURRENT_MOTOR_ACTION == GO_IN){
    backlight_lcd(0);
    /*
     * First :
     * Check the stop switch to stop the motor
     */
    if (read_stop_switch()){
      do_motor_stop();
      CURRENT_MOTOR_ACTION = STOPPED_IN;
      emergency(0);
      /*
       * We can unset the initializing flag as the switch is closed
       */
      IS_INITIALIZING = 0;
      UNDEF_RETRIES = 0;
    } else {
      do_motor_bwd();
    }

    /*
     * These last checks indicates that we are in troubles
     * and manage the motor driver to not destroy mechanism
     * and limit transistors heating
     * If no pulse in a short time range -> motor is mechanically stuck
     * -> STOP !
     */
    if ( FEEDBACK_TIME_TOO_LONG() || TOTAL_TIME_TOO_LONG() ){
      do_motor_stop();
      CURRENT_MOTOR_ACTION = STOPPED_UNDEF;
      MOTOR_UNDEFINED_POS_TIME = millis();
      emergency(1);
    }

    /*
     * Use feedback pulse counter to stop the motor
     * This is an emergency stop in case of switch failure
     */
    if (FEEDBACK_PULSE_MAX_MEM != 0){
      if (FEEDBACK_PULSE_COUNT >= (FEEDBACK_PULSE_MAX_MEM + 15)){
        do_motor_stop();
        CURRENT_MOTOR_ACTION = STOPPED_IN;
        emergency(1);
      }
    }
  }

  if (CURRENT_MOTOR_ACTION == GO_OUT){
    /*
     * Emergency stop the motor if we have no feedback signal change
     * We'll use this method to count how pulse we need to go completely out
     */
    if ( FEEDBACK_TIME_TOO_LONG() || TOTAL_TIME_TOO_LONG() ){
      do_motor_stop();
      CURRENT_MOTOR_ACTION = STOPPED_OUT;
      /*
       * We finally know the max count to open the screen
       */
      if (!TOTAL_TIME_TOO_LONG() && FEEDBACK_PULSE_MAX_MEM == 0){
        FEEDBACK_PULSE_MAX_MEM = FEEDBACK_PULSE_COUNT;
      } else {
        emergency(1);
        CURRENT_MOTOR_ACTION = STOPPED_UNDEF;
      }
    }

    /*
     * Use feedback pulse counter to stop the motor
     */
    if (FEEDBACK_PULSE_MAX_MEM != 0){
      if (FEEDBACK_PULSE_COUNT >= FEEDBACK_PULSE_MAX_MEM){
        do_motor_stop();
        CURRENT_MOTOR_ACTION = STOPPED_OUT;
      }
    }
  }

  if (CURRENT_MOTOR_ACTION == STOPPED_OUT){
    backlight_lcd(1);
  } 

  if (CURRENT_MOTOR_ACTION == STOPPED_UNDEF && ((millis() - MOTOR_UNDEFINED_POS_TIME) > 5000) && UNDEF_RETRIES < 5){
    /*
     * Undefined mechanism position
     * -> Let's retry every 5 seconds 4 times max
     * If still stuck, We cannot do more...
     */
    CURRENT_MOTOR_ACTION = GO_IN;
    UNDEF_RETRIES++;
    MOTOR_UNDEFINED_POS_TIME = millis();
  }
}
